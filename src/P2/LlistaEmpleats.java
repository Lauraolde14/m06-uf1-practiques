package P2;

import pojo.Empleat;

import java.util.ArrayList;
import java.util.List;

public class LlistaEmpleats {

    private List<Empleat> llistaEmpleat = new ArrayList<Empleat>();

    public LlistaEmpleats(){}

    public void add(Empleat e){
        llistaEmpleat.add(e);
    }

    public List<Empleat> getLlistaEmpleats(){
        return llistaEmpleat;
    }
}
