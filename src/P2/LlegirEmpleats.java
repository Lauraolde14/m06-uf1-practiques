package P2;

import com.thoughtworks.xstream.XStream;
import pojo.Empleat;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LlegirEmpleats {
    public static void main(String[] args) throws IOException {
        XStream xstream = new XStream();
        xstream.alias("LlistaEmpleat", LlistaEmpleats.class);
        xstream.alias("DadesEmpleat", Empleat.class);
        xstream.addImplicitCollection(LlistaEmpleats.class, "llistaEmpleat");

        LlistaEmpleats llistarEmpleats = (LlistaEmpleats) xstream.fromXML(new FileInputStream("Empleats.xml"));
        System.out.println("Número empleats: " + llistarEmpleats.getLlistaEmpleats().size());

        List<Empleat> llistaEmpleats = new ArrayList<Empleat>();
        llistaEmpleats = llistarEmpleats.getLlistaEmpleats();

        Iterator iterator = llistaEmpleats.listIterator();
        while (iterator.hasNext()){
            Empleat e = (Empleat) iterator.next();
            System.out.println("Id: " + e.getId() + " cognom: " + e.getCognom() +
                    " Departament: " + e.getDepartament() + " Salari: " + e.getSalari());
        }
        System.out.println("Fi llistat");
    }
}
