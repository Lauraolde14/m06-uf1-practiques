package P2;

import com.thoughtworks.xstream.XStream;
import pojo.Departament;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LlegirDepartaments {
    public static void main(String[] args) throws IOException {
        XStream xstream = new XStream();
        xstream.alias("LlistaDepartament", LlistaDepartaments.class);
        xstream.alias("DadesDepartament", Departament.class);
        xstream.addImplicitCollection(LlistaDepartaments.class, "llistaDepartament");

        LlistaDepartaments llistarDepartaments = (LlistaDepartaments) xstream.fromXML(new FileInputStream("Departaments.xml"));
        System.out.println("Número departaments: " + llistarDepartaments.getLlistaDepartaments().size());

        List<Departament> llistaDepartaments = new ArrayList<Departament>();
        llistaDepartaments = llistarDepartaments.getLlistaDepartaments();

        Iterator iterator = llistaDepartaments.listIterator();
        while (iterator.hasNext()){
            Departament d = (Departament) iterator.next();
            System.out.println("Id: " + d.getId() + " Nom: " + d.getNom() + " Localitat: " + d.getLocalitat());
        }
        System.out.println("Fi llistat");
    }
}
