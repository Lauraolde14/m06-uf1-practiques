package P2;

import pojo.Departament;

import java.util.ArrayList;
import java.util.List;

public class LlistaDepartaments {

    private List<Departament> llistaDepartament = new ArrayList<Departament>();

    public LlistaDepartaments(){}

    public void add(Departament d){
        llistaDepartament.add(d);
    }

    public List<Departament> getLlistaDepartaments(){
        return llistaDepartament;
    }
}
