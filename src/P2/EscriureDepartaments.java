package P2;

import com.thoughtworks.xstream.XStream;
import pojo.Departament;

import java.io.*;

public class EscriureDepartaments {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        File fileDepartaments = new File("FitxerDepartaments.dat");
        FileInputStream filein = new FileInputStream(fileDepartaments);
        ObjectInputStream dataIS = new ObjectInputStream(filein);
        System.out.println("Comença el procés de creació del fitxer XML...");
        LlistaDepartaments llistaDepartaments = new LlistaDepartaments();
        try{
            while (true){
                Departament departament = (Departament) dataIS.readObject();
                llistaDepartaments.add(departament);
            }
        }catch (EOFException eo){ }
        dataIS.close();
        try{
            XStream xstream = new XStream();
            xstream.alias("LlistaDepartament", LlistaDepartaments.class);
            xstream.alias("DadesDepartament", Departament.class);
            xstream.addImplicitCollection(LlistaDepartaments.class, "llistaDepartament");
            xstream.toXML(llistaDepartaments, new FileOutputStream("Departaments.xml"));
            System.out.println("Creant fitxer XML...");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
