package P2;

import com.thoughtworks.xstream.XStream;
import pojo.Empleat;

import java.io.*;

public class EscriureEmpleats {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        File fileEmpleats = new File("FitxerEmpleats.dat");
        FileInputStream filein = new FileInputStream(fileEmpleats);
        ObjectInputStream dataIS = new ObjectInputStream(filein);
        System.out.println("Comença el procés de creació del fitxer XML...");
        LlistaEmpleats llistaEmpleats = new LlistaEmpleats();
        try{
            while (true){
                Empleat empleat = (Empleat) dataIS.readObject();
                llistaEmpleats.add(empleat);
            }
        }catch (EOFException eo){ }
        dataIS.close();
        try{
            XStream xstream = new XStream();
            xstream.alias("LlistaEmpleat", LlistaEmpleats.class);
            xstream.alias("DadesEmpleat", Empleat.class);
            xstream.addImplicitCollection(LlistaEmpleats.class, "llistaEmpleat");
            xstream.toXML(llistaEmpleats, new FileOutputStream("Empleats.xml"));
            System.out.println("Creant fitxer XML...");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
