package P3;

import com.google.gson.*;
import pojo.Product;

import java.io.FileReader;

public class LlegirProduct {
    public static void main(String[] args) {
        JsonParser parser = new JsonParser();
        Gson gson = new Gson();
        try{
            FileReader reader = new FileReader("products.json");
            JsonElement dades = parser.parse(reader);
            JsonArray array = dades.getAsJsonArray();
            java.util.Iterator<JsonElement> iter = array.iterator();
            while (iter.hasNext()) {
                JsonElement entrada = iter.next();
                Product product = gson.fromJson(entrada, Product.class);
                System.out.println(product);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
