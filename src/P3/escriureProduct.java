package P3;

import P1.MiObjectOutputStream;
import com.google.gson.Gson;
import pojo.Categories;
import pojo.Product;

import java.io.*;

public class escriureProduct {
    public static void main(String[] args) {
        try{
            System.out.println("Formant nou fitxer");
            File fileProduct = new File("Products2.json");
            Gson gson = new Gson();
            ObjectOutputStream dataProduct = new MiObjectOutputStream(new FileOutputStream(fileProduct));
            String name [] = {"Xiaomi mi note 10", "Steelseries Arctics 5", "Nintendo Switch", "Playstation 5", "Logitech G513"};
            int price [] = {400, 100, 320, 499, 123};
            int stock [] = {10, 5, 27, 45, 230};
            String picture [] = {"MiNote10.png", "Arctics5.png", "NintendoSwitch.jpeg", "Playstation5.png", "G513.jpeg"};
            int id []  = {10, 11, 12, 13, 14};
            String nameC [] = {"phone", "headphones", "videogames computes", "videogames computer", "keyboard"};

            dataProduct.writeObject("[");
            for(int i = 0; i < 5; i++){
                Product product = new Product(name[i], price[i], stock[i], picture[i], new Categories[]{new Categories(id[i], nameC[i])});
                String json = gson.toJson(product);
                if(i!=4){
                    dataProduct.writeObject(json + "," + "\n");
                }else{
                    dataProduct.writeObject(json);
                }
            }
            dataProduct.writeObject("]");

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
