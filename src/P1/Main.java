package P1;

import pojo.Departament;
import pojo.Empleat;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        File fileEmpleats = new File("FitxerEmpleats.dat");
        File fileDepartamets = new File("FitxerDepartaments.dat");
        if(fileEmpleats.exists()){
            fileEmpleats.delete();
        }
        if(fileDepartamets.exists()){
            fileDepartamets.delete();
        }
        Exercici2();
        Exercici3();
        Exercici4();
        Exercici3();
    }

    public static void Exercici2(){
        try {
            //5 objectes Empleat.
            File fileEmpleats = new File("FitxerEmpleats.dat");
            ObjectOutputStream dataEmpleats = new ObjectOutputStream(new FileOutputStream(fileEmpleats));

            int idEmpleat [] = {1, 2, 3, 4, 5};
            String cognom [] = {"Lopez", "García", "Portella", "Juarez", "Rodríguez"};
            int departament [] = {1, 2, 3, 4, 5};
            double salari [] = {1024.5, 2005.9, 1370.3, 900.7, 1689};

            System.out.println("Pujant dades empleat");
            for(int i = 0; i < idEmpleat.length; i++){
                Empleat empleat;
                empleat = new Empleat(idEmpleat[i], cognom[i], departament[i], salari[i]);
                dataEmpleats.writeObject(empleat);
            }
            dataEmpleats.close();

            //5 objectes Departament.
            File fileDepartaments = new File("FitxerDepartaments.dat");
            ObjectOutputStream dataDepartaments = new ObjectOutputStream(new FileOutputStream(fileDepartaments));

            int idDepartament [] = {1, 2, 3, 4, 5};
            String nom [] = {"Marqueting", "Logística", "Recursos humans", "Ventes", "Finances"};
            String localitat [] = {"Barcelona", "Madrid", "Girona", "Tarragona", "Tarrasa"};

            System.out.println("Pujant dades departament");
            for(int i = 0; i < idDepartament.length; i++){
                Departament Departament;
                Departament = new Departament(idDepartament[i], nom[i], localitat[i]);
                dataDepartaments.writeObject(Departament);
            }

        }catch (FileNotFoundException fn){
            System.out.println("No s'ha trobat el fitxer");
            fn.printStackTrace();
        }catch (IOException io){
            System.out.println("Error al escriure sobre el fitxer");
            io.printStackTrace();
        }catch (Exception e){
            System.out.println("Error: " + e);
            e.printStackTrace();
        }
    }

    public static void Exercici3(){
        //Llegir Empleats
        try {
            System.out.println("Llegint dades empleat: ");
            Empleat empleat;
            File fileEmpleats = new File("FitxerEmpleats.dat");
            ObjectInputStream dataEmpleats = new ObjectInputStream(new FileInputStream(fileEmpleats));
            int i = 1;
            while(true){
                empleat = (Empleat) dataEmpleats.readObject();
                System.out.println(i + "=>");
                i++;
                System.out.println(empleat);
            }

        }catch (EOFException eo){
            System.out.println("Fi lectura");
        }catch (StreamCorruptedException x){
        }catch (Exception e){
            System.out.println("Error: " + e);
            e.printStackTrace();
        }

        //Llegir Departaments
        try{
            System.out.println("Llegint dades departament: ");
            Departament departament;
            File fileDepartamets = new File("FitxerDepartaments.dat");
            ObjectInputStream dataDepartaments = new ObjectInputStream(new FileInputStream(fileDepartamets));
            int j = 1;
            while(true){
                departament = (Departament) dataDepartaments.readObject();
                System.out.println(j + "=>");
                j++;
                System.out.println(departament);
            }
        }catch (EOFException eo){
            System.out.println("Fi lectura");
        }catch (StreamCorruptedException x){
        }catch (Exception e){
            System.out.println("Error: " + e);
            e.printStackTrace();
        }

    }

    public static void Exercici4(){
        try {
            //5 objectes Empleat.
            System.out.println("Afegint més dades empleat");
            File fileEmpleats = new File("FitxerEmpleats.dat");
            if(fileEmpleats.exists()){
                ObjectOutputStream dataEmpleats = new MiObjectOutputStream(new FileOutputStream(fileEmpleats, true));

                int idEmpleat [] = {6, 7, 8, 9, 10};
                String cognom [] = {"Ortega", "Delgado", "Castillo", "Moreno", "Díaz"};
                int departament [] = {6, 7, 8, 9, 10};
                double salari [] = {1064.5, 1005.9, 1850.3, 1200.7, 1359};

                for(int i = 0; i < idEmpleat.length; i++){
                    Empleat empleat;
                    empleat = new Empleat(idEmpleat[i], cognom[i], departament[i], salari[i]);
                    dataEmpleats.writeObject(empleat);
                }
                dataEmpleats.close();
            }

            //5 objectes Departament.
            System.out.println("Afegint més dades departament");
            File fileDepartaments = new File("FitxerDepartaments.dat");
            if(fileDepartaments.exists()){
                ObjectOutputStream dataDepartaments = new MiObjectOutputStream(new FileOutputStream(fileDepartaments, true));

                int idDepartament [] = {6, 7, 8, 9, 10};
                String nom [] = {"Manteniment", "Vendes", "Recursos humans", "Ventes", "Finances"};
                String localitat [] = {"Barcelona", "Madrid", "Girona", "Tarragona", "Tarrasa"};

                for(int i = 0; i < idDepartament.length; i++){
                    Departament Departament;
                    Departament = new Departament(idDepartament[i], nom[i], localitat[i]);
                    dataDepartaments.writeObject(Departament);
                }
            }

        }catch (FileNotFoundException fn){
            System.out.println("No s'ha trobat el fitxer");
            fn.printStackTrace();
        }catch (IOException io){
            System.out.println("Error al escriure sobre el fitxer");
            io.printStackTrace();
        }catch (Exception e){
            System.out.println("Error: " + e);
            e.printStackTrace();
        }
    }
}


