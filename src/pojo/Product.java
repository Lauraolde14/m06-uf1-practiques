package pojo;

import java.util.ArrayList;
import java.util.Arrays;

public class Product {
    private String name;
    private int price;
    private int stock;
    private String picture;
    private Categories[] categories;

    public Product(String name, int price, int stock, String picture, Categories[] categories) {
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.picture = picture;
        this.categories = categories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Categories[] getCategories() {
        return categories;
    }

    public void setCategories(Categories[] categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "Product{" +
                " name= " + name + '\'' +
                ", price= " + price +
                ", stock= " + stock +
                ", picture= " + picture + '\'' +
                ", categories= " + Arrays.toString(categories) +
                '}';
    }
}
